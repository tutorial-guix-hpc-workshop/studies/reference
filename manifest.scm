(specifications->manifest
  (list 
   "minisolver"

   "r"
   "r-ggplot2"

   "emacs"
   "emacs-org"
   "emacs-org-ref"
   "emacs-ess"
   "emacs-fill-column-indicator"
   "texlive-scheme-basic"
   "texlive-hyperref"
   "texlive-geometry"
   "texlive-ec"
   "texlive-float"
   "texlive-latexmk"
   "texlive-wrapfig"
   "texlive-amsmath"
   "texlive-ulem"
   "texlive-capt-of"
   "texlive-biblatex"
   "texlive-biber"

   "bash"
   "sed"
   "coreutils"))
