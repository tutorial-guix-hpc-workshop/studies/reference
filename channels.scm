(list
 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (commit "10f3dd0e9e06d71d1bc1615c6a60cc3aa1ad1ff4"))

 (channel
  (name 'minichannel)
  (url
   "https://gitlab.inria.fr/tutorial-guix-hpc-workshop/software/minichannel")
  (commit "53049fa178ae3a93942554e7fed0d1ccc7901c9e")))
